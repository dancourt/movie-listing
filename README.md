### Hi
Hi, this is my (Dan Court) attempt at the Movie Listing challenge (probably not that great! But I'm happy it's working!!!)

Some notes below on how to run the app etc. I've also uploaded it to this URL: [www.danielcourt.co.uk/movies](https://www.danielcourt.co.uk/movies/)

### Solution
The app uses React and Redux and I used Create React App to save time on the initial creation.

To get the list of movies and genres I've created selectors to merge the different APIs and to get the results required based on the filtering. I've used the reselect library on this section [https://github.com/reduxjs/reselect](https://github.com/reduxjs/reselect)

This has only been tested in Chrome and no other device or browser (due to time).

I don't think the design will be winning any awards!!!

### Running the app
### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.
