import axios from 'axios';
import { FETCH_MOVIES, FETCH_CONFIG, FETCH_GENRES, UPDATE_RATING, TOGGLE_GENRE } from './types';

const API_KEY = 'b1e51ec2a506001c8e7a32c70436c534'

//actions to dispatch the API requests
export const fetchMovies = () => async dispatch => {
    const res = await axios.get(`https://api.themoviedb.org/3/movie/now_playing?page=1&language=en-US&api_key=${API_KEY}&sort_by=popularity.desc`);
    dispatch({ type: FETCH_MOVIES, payload: res.data });
};

export const fetchConfig = () => async dispatch => {
    const res = await axios.get(`https://api.themoviedb.org/3/configuration?api_key=${API_KEY}`);
    dispatch({ type: FETCH_CONFIG, payload: res.data });
};

export const fetchGenres = () => async dispatch => {
    const res = await axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}`);
    dispatch({ type: FETCH_GENRES, payload: res.data });
};


//action to update select rating on filter
export const updateRating = (rating) => {
    return {
        type: UPDATE_RATING,
        rating
    }
}

//action to toggle the selected genres on the filter
export const toggleGenre = (id) => {
    return {
        type: TOGGLE_GENRE,
        id
    }
}
