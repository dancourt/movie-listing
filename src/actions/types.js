export const FETCH_MOVIES = 'fetch_movies';
export const FETCH_CONFIG = 'fetch_config';
export const FETCH_GENRES = 'fetch_genres';
export const UPDATE_RATING = 'update_rating';
export const TOGGLE_GENRE = 'toggle_genre';

