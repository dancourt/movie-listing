import { combineReducers } from 'redux';
import moviesReducer from './moviesReducer';
import configReducer from './configReducer';
import genreReducer from './genreReducer';
import ratingReducer from './ratingReducer';
import searchGenreReducer from './searchGenreReducer';

export default combineReducers({
    config: configReducer,
    movies: moviesReducer,
    movieGenres: genreReducer,
    rating: ratingReducer,
    searchGenre: searchGenreReducer
});