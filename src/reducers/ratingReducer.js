import { UPDATE_RATING } from '../actions/types';

export default function(state = null, action) {
    switch (action.type) {
        case UPDATE_RATING:
            return action.rating || '3';
        default:
            return state;
    }
}