import { TOGGLE_GENRE } from '../actions/types';

export default function(state = [], action) {
    switch (action.type) {
        case TOGGLE_GENRE:
            //check to see if genre has already been checked, if it has remove form state
            let genreAlreadyExists = state.indexOf(action.id) > -1;
            if(genreAlreadyExists){
                return state.filter(genre => genre !== action.id);
            } else{
                return [
                    ...state,
                    action.id
                ]
            }
        default:
            return state;
    }
}