import { FETCH_GENRES } from '../actions/types';

export default function(state = null, action) {
    switch (action.type) {
        case FETCH_GENRES:
            return action.payload || false;
        default:
            return state;
    }
}