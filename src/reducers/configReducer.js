import { FETCH_CONFIG } from '../actions/types';

export default function(state = null, action) {
    switch (action.type) {
        case FETCH_CONFIG:
            return action.payload || false;
        default:
            return state;
    }
}