//global header file
import React from 'react';
import styles from './Header.module.scss'; 

const Header = () => {
    return (
        <header className={styles.mainHeader}>
            <h1 className={styles.mainTitle}>Movie Listing<span className={styles.subTitle}>Now showing</span></h1>
        </header>
    )
}

export default Header;