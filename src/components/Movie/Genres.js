//display individual movie genres
import React, { Component } from 'react';

class Genres extends Component {
    renderGenres() {
        if(this.props.content){
            return (                
                 this.props.content.map(genre => {
                    return (
                        <li key={genre.id}>{genre.name}</li>
                    )
                })
            );  
        } else {
            return (
                <div className="loading">Loading</div>
            );
        }
    }

    render(){
        return (
            <ul>
                {this.renderGenres()}
            </ul>
        )
    }
}

export default Genres;