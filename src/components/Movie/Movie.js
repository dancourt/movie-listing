//individual movie component
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchConfig } from '../../actions';
import Genres from './Genres';
import styles from './Movie.module.scss'; 

class Movie extends Component {
    componentDidMount() {
        this.props.fetchConfig();
    }

    renderMovie() {
        if(this.props.details && this.props.config){
            const movieDetails = this.props.details;
            //get image url from state config
            const baseImageURL = this.props.config.images.base_url;
                
            return (                
                <div className={styles.movie} key={movieDetails.id}>
                    <img className={styles.poster} src={`${baseImageURL}/w780/${movieDetails.poster_path}`} alt={`${movieDetails.title} poster`} />
                    <div className={styles.information}>
                        <h5 className={styles.title}>{movieDetails.title}</h5>
                        <div className={styles.genres}>
                            <Genres content={movieDetails.genres} />
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="loading">Loading</div>
            );
        }
    }

    render(){
        return (
            <div className="cell">
                {this.renderMovie()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        config: state.config
    }
  }

export default connect(mapStateToProps, { fetchConfig })(Movie);