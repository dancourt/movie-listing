//compoennt displays the list of ratings in the filter section
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateRating } from '../../actions';


class RatingFilter extends Component {
    componentDidMount() {
        this.props.updateRating();
    }

    render(){
        //TO-DO add this to a loop rather than manual list
        return (
            <div>
                <h4>Rating</h4>
                <div>
                    <button onClick={() => this.props.updateRating('0')} className={`ratingItem ${this.props.rating === '0' ? 'ratingItem--active' : ''}`}>0</button>
                    <button onClick={() => this.props.updateRating('0.5')} className={`ratingItem ${this.props.rating === '0.5' ? 'ratingItem--active' : ''}`}>0.5</button>
                    <button onClick={() => this.props.updateRating('1')} className={`ratingItem ${this.props.rating === '1' ? 'ratingItem--active' : ''}`}>1</button>
                    <button onClick={() => this.props.updateRating('1.5')} className={`ratingItem ${this.props.rating === '1.5' ? 'ratingItem--active' : ''}`}>1.5</button>
                    <button onClick={() => this.props.updateRating('2')} className={`ratingItem ${this.props.rating === '2' ? 'ratingItem--active' : ''}`}>2</button>
                    <button onClick={() => this.props.updateRating('2.5')} className={`ratingItem ${this.props.rating === '2.5' ? 'ratingItem--active' : ''}`}>2.5</button>
                    <button onClick={() => this.props.updateRating('3')} className={`ratingItem ${this.props.rating === '3' ? 'ratingItem--active' : ''}`}>3</button>
                    <button onClick={() => this.props.updateRating('3.5')} className={`ratingItem ${this.props.rating === '3.5' ? 'ratingItem--active' : ''}`}>3.5</button>
                    <button onClick={() => this.props.updateRating('4')} className={`ratingItem ${this.props.rating === '4' ? 'ratingItem--active' : ''}`}>4</button>
                    <button onClick={() => this.props.updateRating('4.5')} className={`ratingItem ${this.props.rating === '4.5' ? 'ratingItem--active' : ''}`}>4.5</button>
                    <button onClick={() => this.props.updateRating('5')} className={`ratingItem ${this.props.rating === '5' ? 'ratingItem--active' : ''}`}>5</button>
                    <button onClick={() => this.props.updateRating('5.5')} className={`ratingItem ${this.props.rating === '5.5' ? 'ratingItem--active' : ''}`}>5.5</button>
                    <button onClick={() => this.props.updateRating('6')} className={`ratingItem ${this.props.rating === '6' ? 'ratingItem--active' : ''}`}>6</button>
                    <button onClick={() => this.props.updateRating('6.5')} className={`ratingItem ${this.props.rating === '6.5' ? 'ratingItem--active' : ''}`}>6.5</button>
                    <button onClick={() => this.props.updateRating('7')} className={`ratingItem ${this.props.rating === '7' ? 'ratingItem--active' : ''}`}>7</button>
                    <button onClick={() => this.props.updateRating('7.5')} className={`ratingItem ${this.props.rating === '7.5' ? 'ratingItem--active' : ''}`}>7.5</button>
                    <button onClick={() => this.props.updateRating('8')} className={`ratingItem ${this.props.rating === '8' ? 'ratingItem--active' : ''}`}>8</button>
                    <button onClick={() => this.props.updateRating('8.5')} className={`ratingItem ${this.props.rating === '8.5' ? 'ratingItem--active' : ''}`}>8.5</button>
                    <button onClick={() => this.props.updateRating('9')} className={`ratingItem ${this.props.rating === '9' ? 'ratingItem--active' : ''}`}>9</button>
                    <button onClick={() => this.props.updateRating('9.5')} className={`ratingItem ${this.props.rating === '9.5' ? 'ratingItem--active' : ''}`}>9.5</button>
                    <button onClick={() => this.props.updateRating('10')} className={`ratingItem ${this.props.rating === '10' ? 'ratingItem--active' : ''}`}>10</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        rating: state.rating
    }
  }

export default connect(mapStateToProps, { updateRating })(RatingFilter);