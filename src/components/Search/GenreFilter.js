//Loops through genres available in the state and prints out checkbox
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGenres, toggleGenre } from '../../actions';
import { movieGenreList } from '../../selectors/genre_list';

import styles from './Search.module.scss';

class GenreFilter extends Component {
    renderGenres(){
        if(this.props.availableGenres){
            return this.props.availableGenres.map(genre => {
                return (
                    <div key={`genreFilter-${genre.id}`} className={styles.genre}>
                        <input type="checkbox" id={`genreFilter-${genre.id}`}  onClick={() => this.props.toggleGenre(genre.id)} />
                        <label htmlFor={`genreFilter-${genre.id}`}>{genre.name}</label>
                    </div>

                )
            })
        }
    }

    render(){
        return (
            <div>
                <h4>Genre</h4> 
                <div className={styles.genres}>{this.renderGenres()}</div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        availableGenres: movieGenreList(state),
        searchGenre: state.searchGenre
    }
  }

export default connect(mapStateToProps, { fetchGenres, toggleGenre })(GenreFilter);
