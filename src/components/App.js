//  global app file
import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import Header from './Header/Header';
import MovieList from './MovieList/MovieList';
import GenreFilter from './Search/GenreFilter';
import RatingFilter from './Search/RatingFilter';

import styles from '../assets/sass/App.module.scss';

class App extends Component {
    render(){
        return (
            <div>   
                <Header/>
                <div className={styles.searchFilter}>
                    <div className={styles.title}>Filter</div>
                    <RatingFilter/>
                    <GenreFilter/>
                </div>
                <MovieList/>
            </div>
        );
    }
}

export default connect(null, actions)(App);