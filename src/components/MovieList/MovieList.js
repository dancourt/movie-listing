//main movie list component - display the list of filtered movies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchMovies, fetchGenres } from '../../actions';
import { movieSelectorGenres } from '../../selectors/movie_list';

import Movie from '../Movie/Movie';

class MovieList extends Component {
    componentDidMount() {
        this.props.fetchMovies();
        this.props.fetchGenres();
    }

    renderMovies() {
        if(this.props.basicMovieDetails){  
            if(this.props.basicMovieDetails.length == '0'){
                return(
                    <h3>There are no results for your search</h3>
                )
            } else{
                return this.props.basicMovieDetails.map(movie => {
                    return (
                        <Movie details={movie} key={movie.id}/>
                    )
                });
            }       

        } else {
            return (
                <div className="loading">Loading</div>
            );
        }
    }

    render(){
        return (
            <div className="container">
                {this.renderMovies()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        basicMovieDetails: movieSelectorGenres(state),
        searchGenre: state.searchGenre
    }
  }

export default connect(mapStateToProps, { fetchMovies, fetchGenres })(MovieList);