//takes movies and genres and merge them in one seperate clean state just for movies
import { createSelector } from 'reselect';
const movieSelector = state => state.movies; 
const movieGenresSelector = state => state.movieGenres; 
const ratingSelector = state => state.rating;
const genreSelector = state => state.searchGenre; 

export const movieSelectorGenres = createSelector(
    [movieSelector, movieGenresSelector, ratingSelector, genreSelector], 
    (movies, movieGenres, rating, searchGenre) => {

    if(movies && movieGenres && rating && searchGenre){
        //get movie rating from state (default 3)
        const minimumRating = rating;
        //order movies by popularity
        const sortedMovies = movies.results.sort((a, b) => (b.popularity > a.popularity) ? 1 : -1);
        //filter out movies below rating set above
        const ratedMovies = sortedMovies.filter(movie => movie.vote_average >= minimumRating);

        //loop through movies and find all movies that match the selected genres
        const filteredMovies = ratedMovies.filter(movie => 
            searchGenre.every(movieGenre => movie.genre_ids.includes(movieGenre))
        );

        return filteredMovies.map(movie => {
            const movieGenresList = movie.genre_ids.map(movieGenre => {
                const thisGenre = movieGenres.genres.find(genre => genre.id === movieGenre);
                return thisGenre
            })

            const movieDetails = {
                id: movie.id,
                title: movie.title,
                poster_path: movie.poster_path,
                genres: movieGenresList,
                popularity: movie.popularity,
                vote_average: movie.vote_average
            }

            return movieDetails;
        })
    }    
});
