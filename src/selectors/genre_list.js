//takes genres, get unique genres, sorts them and then filter to leave genres that are linked to visible films
import { createSelector } from 'reselect';
const movieSelector = state => state.movies;
const movieGenresSelector = state => state.movieGenres; 
const ratingSelector = state => state.rating;
const genreSelector = state => state.searchGenre; 

export const movieGenreList = createSelector(
    [movieSelector, movieGenresSelector, ratingSelector, genreSelector], 
    (movies, movieGenres, rating, searchGenre) => {

    if(movies && movieGenres && rating && searchGenre){
        //get movie rating from state (default 3)
        const minimumRating = rating;
        //filter out movies below rating set above
        const ratedMovies = movies.results.filter(movie => movie.vote_average >= minimumRating);

        //loop through movies and find all movies that match the selected genres
        const filteredMovies = ratedMovies.filter(movie => 
            searchGenre.every(movieGenre => movie.genre_ids.includes(movieGenre))
        );
       
        //get all genres related to the movies in the current state
        const genreList = filteredMovies.map(movie => {
            const movieGenresList = movie.genre_ids.map(movieGenre => {
                const thisGenre = movieGenres.genres.find(genre => genre.id === movieGenre);
                return thisGenre
            })
            return movieGenresList
        })

        //flatten list of genres and use Set to remove duplicates and create an array from results and order by name
        const flattenedGenreList = [].concat(...genreList);  
        const uniqueGenres = new Set(flattenedGenreList);
        let genreArray = [...uniqueGenres];
        genreArray = genreArray.sort((a, b) => (a.name > b.name) ? 1 : -1);

        //loop through final array of genres to add to state
        return genreArray.map(genre => {
            const genreDetails = {
                id: genre.id,
                name: genre.name,
            }

            return genreDetails;
        })
    }    
});
